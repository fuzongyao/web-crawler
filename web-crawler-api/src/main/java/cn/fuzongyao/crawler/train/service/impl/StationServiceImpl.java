package cn.fuzongyao.crawler.train.service.impl;

import cn.fuzongyao.crawler.train.entity.StationDO;
import cn.fuzongyao.crawler.train.repository.IStationRepository;
import cn.fuzongyao.crawler.train.service.IStationService;
import cn.fuzongyao.crawler.util.JsoupUtils;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * @author fuzongyao
 * @date 2020/09/21
 * @since 1.0.0
 */
@Slf4j
@Service
public class StationServiceImpl implements IStationService {

    @Autowired
    private IStationRepository stationRepository;

    private static final String[] STATION_URLS = {
            // 哈尔滨局
            "https://www.12306.cn/mormhweb/kyyyz/ky_heb/201001/t20100124_1161.html",
            // 沈阳局
            "https://www.12306.cn/mormhweb/kyyyz/shenyang/201001/t20100124_1164.html",
            // 北京局
            "https://www.12306.cn/mormhweb/kyyyz/beijing/201001/t20100124_1166.html",
            // 太原局
            "https://www.12306.cn/mormhweb/kyyyz/taiyuan/201001/t20100124_1168.html",
            // 呼和浩特局
            "https://www.12306.cn/mormhweb/kyyyz/heht/201001/t20100124_1170.html",
            // 郑州局
            "https://www.12306.cn/mormhweb/kyyyz/zhengzhou/201001/t20100124_1172.html",
            // 武汉局
            "https://www.12306.cn/mormhweb/kyyyz/wuhan/201001/t20100124_1174.html",
            // 西安局
            "https://www.12306.cn/mormhweb/kyyyz/xian/201001/t20100124_1176.html",
            // 济南局
            "https://www.12306.cn/mormhweb/kyyyz/jinan/201001/t20100124_1178.html",
            // 上海局
            "https://www.12306.cn/mormhweb/kyyyz/shanghai/201001/t20100124_1180.html",
            // 南昌局
            "https://www.12306.cn/mormhweb/kyyyz/nanchang/201001/t20100124_1182.html",
            // 广州局
            "https://www.12306.cn/mormhweb/kyyyz/guangtie/201001/t20100124_1184.html",
            // 南宁局
            "https://www.12306.cn/mormhweb/kyyyz/nanning/201001/t20100124_1186.html",
            // 成都局
            "https://www.12306.cn/mormhweb/kyyyz/chengdu/201001/t20100124_1188.html",
            // 昆明局
            "https://www.12306.cn/mormhweb/kyyyz/kunming/201001/t20100124_1190.html",
            // 兰州局
            "https://www.12306.cn/mormhweb/kyyyz/lanzhou/201001/t20100124_1192.html",
            // 乌鲁木齐局
            "https://www.12306.cn/mormhweb/kyyyz/wlmq/201001/t20100124_1194.html",
            // 青藏局
            "https://www.12306.cn/mormhweb/kyyyz/qinzang/201001/t20100124_1196.html"
    };

    private static final String[] STATION_CJS_URLS = {
            // 哈尔滨局
            "https://www.12306.cn/mormhweb/kyyyz/ky_heb/201001/t20100124_1162.html",
            // 沈阳局
            "https://www.12306.cn/mormhweb/kyyyz/shenyang/201001/t20100124_1163.html",
            // 北京局
            "https://www.12306.cn/mormhweb/kyyyz/beijing/201001/t20100124_1165.html",
            // 太原局
            "https://www.12306.cn/mormhweb/kyyyz/taiyuan/201001/t20100124_1167.html",
            // 呼和浩特局
            "https://www.12306.cn/mormhweb/kyyyz/heht/201001/t20100124_1169.html",
            // 郑州局
            "https://www.12306.cn/mormhweb/kyyyz/zhengzhou/201001/t20100124_1171.html",
            // 武汉局
            "https://www.12306.cn/mormhweb/kyyyz/wuhan/201001/t20100124_1173.html",
            // 西安局
            "https://www.12306.cn/mormhweb/kyyyz/xian/201001/t20100124_1175.html",
            // 济南局
            "https://www.12306.cn/mormhweb/kyyyz/jinan/201001/t20100124_1177.html",
            // 上海局
            "https://www.12306.cn/mormhweb/kyyyz/shanghai/201001/t20100124_1179.html",
            // 南昌局
            "https://www.12306.cn/mormhweb/kyyyz/nanchang/201001/t20100124_1181.html",
            // 广州局
            "https://www.12306.cn/mormhweb/kyyyz/guangtie/201001/t20100124_1183.html",
            // 南宁局
            "https://www.12306.cn/mormhweb/kyyyz/nanning/201001/t20100124_1185.html",
            // 成都局
            "https://www.12306.cn/mormhweb/kyyyz/chengdu/201001/t20100124_1187.html",
            // 昆明局
            "https://www.12306.cn/mormhweb/kyyyz/kunming/201001/t20100124_1189.html",
            // 兰州局
            "https://www.12306.cn/mormhweb/kyyyz/lanzhou/201001/t20100124_1191.html",
            // 乌鲁木齐局
            "https://www.12306.cn/mormhweb/kyyyz/wlmq/201001/t20100124_1193.html",
            // 青藏局
            "https://www.12306.cn/mormhweb/kyyyz/qinzang/201001/t20100124_1195.html",
    };

    /**
     * 初始化数据
     */
    @Override
    public void init() {
        for (String url : STATION_URLS) {
            init(url);
        }
        for (String url : STATION_CJS_URLS) {
            init(url);
        }
    }

    private void init(String url) {
        Document document = JsoupUtils.parse(url, StandardCharsets.UTF_8);
        if (document == null) {
            return;
        }

        // 解析数据，第1、第2行是表头，第3行开始是数据，第1列是站点名称，第2列是车站地址
        Elements trs = document.select("tbody tr");
        List<StationDO> stationDos = new ArrayList<>(trs.size() - 2);
        for (int i = 3; i < trs.size(); i++) {
            Element element = trs.get(i);
            Elements tds = element.select("td");
            String name = tds.get(0).text();
            String address = tds.get(1).text();
            log.info("name:{}, address:{}", name, address);
            name = name.split("（")[0];
            StationDO stationDO = new StationDO().setName(name).setAddress(address);
            stationDos.add(stationDO);
        }
        stationRepository.saveBatch(stationDos);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
