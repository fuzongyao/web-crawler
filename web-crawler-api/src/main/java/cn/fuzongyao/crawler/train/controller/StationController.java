package cn.fuzongyao.crawler.train.controller;

import cn.fuzongyao.crawler.train.service.IStationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author fuzongyao
 * @date 2020/09/21
 * @since 1.0.0
 */
@RestController
@RequestMapping(value = "/station")
public class StationController {

    @Autowired
    private IStationService stationService;

    @GetMapping("/init")
    public String index() {
        stationService.init();
        return "成功";
    }

}
