package cn.fuzongyao.crawler.train.service;

/**
 * @author fuzongyao
 * @date 2020/09/21
 * @since 1.0.0
 */
public interface IStationService {

    /**
     * 初始化数据
     */
    void init();

}
