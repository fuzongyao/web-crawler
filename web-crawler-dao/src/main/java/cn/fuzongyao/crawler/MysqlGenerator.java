package cn.fuzongyao.crawler;

/**
 * Copyright (c) 2011-2016, hubin (jobob@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.TemplateConfig;
import com.baomidou.mybatisplus.generator.config.po.TableFill;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 代码生成器，参考源码测试用例
 * </p>
 * 具体参考https://gitee.com/baomidou/mybatis-plus<br>
 * /mybatis-plus-generator/src/test/java/com/baomidou/mybatisplus/test/generator/MysqlGenerator.java
 *
 * @author hubin
 * @since 2016-12-01
 */
public class MysqlGenerator {
    /**
     * 表前缀，生成实体类的时候会去掉前缀
     */
    private String[] tablePrefixs = new String[]{"oc_", "c_", "sc_", "ct_"};
    /**
     * 工程目录
     */
    private String projectDir = System.getProperty("user.dir") + File.separator + "web-crawler-dao";
    /**
     * 逻辑删除字段
     */
    private String logicDeleteFieldName = "is_deleted";
    /**
     * 自定义需要填充的字段，配合MetaObjectHandler使用，比如插入数据的时候，把入库时间填充
     */
    private List<TableFill> tableFillList = new ArrayList<>();
    /**
     * 需要生成的表
     */
    private String[] includeTables = {"ct_station"};

    public void generator() {
        // 自定义需要填充的字段，配合MetaObjectHandler使用，比如插入数据的时候，把入库时间填充
        tableFillList.add(new TableFill("create_time", FieldFill.INSERT));
        tableFillList.add(new TableFill("update_time", FieldFill.INSERT_UPDATE));

        // 代码生成器
        AutoGenerator autoGenerator = new AutoGenerator();

        // 全局配置
        GlobalConfig globalConfig = new GlobalConfig()
                .setOutputDir(projectDir + "/src/main/java")
                .setEntityName("%sDO")
                .setServiceName("I%sRepository")
                .setServiceImplName("%sRepositoryImpl")
                .setAuthor("fuzongyao")
                .setSwagger2(true)
                .setFileOverride(true)
                .setOpen(false);
        autoGenerator.setGlobalConfig(globalConfig);

        // 数据源配置
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        dataSourceConfig.setUrl("jdbc:mysql://localhost:3306/common?characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&serverTimezone=GMT%2B8");
        dataSourceConfig.setDriverName("com.mysql.cj.jdbc.Driver");
        dataSourceConfig.setUsername("root");
        dataSourceConfig.setPassword("123456");
        autoGenerator.setDataSource(dataSourceConfig);

        // 包配置
        PackageConfig packageConfig = new PackageConfig()
                .setModuleName("train")
                .setService("repository")
                .setServiceImpl("repository.impl")
                .setParent("cn.fuzongyao.crawler");
        autoGenerator.setPackageInfo(packageConfig);

        autoGenerator.setTemplate(new TemplateConfig().setXml(null).setController(null));
//        autoGenerator.setTemplate(new TemplateConfig().setXml(null).setController(null).setMapper(null).setService(null)
//                .setServiceImpl(null));

        // 策略配置
        StrategyConfig strategyConfig = new StrategyConfig()
                // 表名前缀
                .setTablePrefix(tablePrefixs)
                // 表名生成策略，这里是驼峰
                .setNaming(NamingStrategy.underline_to_camel)
                .setColumnNaming(NamingStrategy.underline_to_camel)
                // 需要生成的表Include
                .setInclude(includeTables)
                // 排除生成的表
                // .setExclude(new String[]{"test"})
                // 自定义实体，公共字段
                // .setSuperEntityColumns(new String[]{})
                .setTableFillList(tableFillList)
                // 逻辑删除字段
                .setLogicDeleteFieldName(logicDeleteFieldName)
                // 自定义实体父类
                // .setSuperEntityClass("com.baomidou.demo.TestEntity")
                // 自定义 mapper 父类
                // .setSuperMapperClass("com.baomidou.demo.TestMapper")
                // 自定义 service 父类
                // .setSuperServiceClass("com.baomidou.demo.TestService")
                // 自定义 service 实现类父类
                // .setSuperServiceImplClass("com.baomidou.demo.TestServiceImpl")
                // 自定义 controller 父类
//                .setSuperControllerClass("com.wt.shejian.controller.BaseController")
                // 【实体】是否为构建者模型（默认 false）
                .setEntityBuilderModel(true)
                .setEntityLombokModel(true)
                // Boolean类型字段是否移除is前缀处理
                .setEntityBooleanColumnRemoveIsPrefix(true)
                // .setControllerMappingHyphenStyle(true)
                .setRestControllerStyle(true);
        autoGenerator.setStrategy(strategyConfig);
        autoGenerator.execute();
    }

    public static void main(String[] args) {
        new MysqlGenerator().generator();
    }

}
