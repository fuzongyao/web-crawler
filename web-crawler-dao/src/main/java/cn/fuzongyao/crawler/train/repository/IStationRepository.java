package cn.fuzongyao.crawler.train.repository;

import cn.fuzongyao.crawler.train.entity.StationDO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 列车站点表 服务类
 * </p>
 *
 * @author fuzongyao
 * @since 2020-09-21
 */
public interface IStationRepository extends IService<StationDO> {

}
