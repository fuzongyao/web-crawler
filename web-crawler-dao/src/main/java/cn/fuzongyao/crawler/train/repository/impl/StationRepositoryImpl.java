package cn.fuzongyao.crawler.train.repository.impl;

import cn.fuzongyao.crawler.train.entity.StationDO;
import cn.fuzongyao.crawler.train.mapper.StationMapper;
import cn.fuzongyao.crawler.train.repository.IStationRepository;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 列车站点表 服务实现类
 * </p>
 *
 * @author fuzongyao
 * @since 2020-09-21
 */
@Service
public class StationRepositoryImpl extends ServiceImpl<StationMapper, StationDO> implements IStationRepository {

}
