package cn.fuzongyao.crawler.train.mapper;

import cn.fuzongyao.crawler.train.entity.StationDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 列车站点表 Mapper 接口
 * </p>
 *
 * @author fuzongyao
 * @since 2020-09-21
 */
public interface StationMapper extends BaseMapper<StationDO> {

}
